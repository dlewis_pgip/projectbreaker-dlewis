using UnityEngine;

public class Bricks : MonoBehaviour
{
    public SpriteRenderer spriteRenderer { get; private set; }
    public int health { get; private set; }
    public Sprite[] states = new Sprite[0];      //creates array of sprite states that changes based on brick health
    public bool unbreakable;                    //sets flag for unrbeakable bricks
    public int points = 100;


    private void Awake()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
    }   

    private void Start()
    {
        if (!this.unbreakable)                  //for bricks not flagged unbreakable
        {
            this.health = this.states.Length;   //the health of the brick is determined by the number of sprite states
            this.spriteRenderer.sprite = this.states[this.health - 1];      //as health decreases, brick sprite state decreases to previous array index
        }
    }

    private void Hit()
    {
        if (this.unbreakable){      //if brick is unbreakable, return out of Hit method without doing anything
            return;
        }
        this.health--;                          //decrease health by 1
        if (this.health <= 0) {                 //if brick health <= 0, hide the brick (destroyed)
            this.gameObject.SetActive(false);
        } else {
        this.spriteRenderer.sprite = this.states[this.health - 1];      //brick sprite index state = health - 1
        }  

        FindObjectOfType<GameManager>().Hit(this);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            Hit();
        }
    }


}


