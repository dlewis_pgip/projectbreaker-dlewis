using UnityEngine;

public class Ball : MonoBehaviour
{
    public new Rigidbody2D rigidbody { get; private set; }
    public float speed = 500f;      //adds speed multiplier to make ball move faster

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        ResetBall();
    }

    public void ResetBall()
    {
        transform.position = Vector2.zero;
        rigidbody.velocity = Vector2.zero;
        Invoke(nameof(SetRandomTrajectory), 1f);        //creates a small delay before ball drops when game starts
    }

    private void SetRandomTrajectory()
    {
        Vector2 force = new Vector2();
        force.x = Random.Range(-1f, 1f);    //sets the ball to drop randomly in a range left to right
        force.y = -1f;                      //sets the ball to always drop down (not up)

        rigidbody.AddForce(force.normalized * speed);     //Normalized to keep force to be greater than 1 (too fast)
    }

    //This method is supposed to fix physics issue where ball can slow down, but it introduces a bug that causes ball to drop instantly at start
    private void FixedUpdate()
    {
        rigidbody.velocity = rigidbody.velocity.normalized * speed;
    }

}
