using UnityEngine;
using UnityEngine.SceneManagement; //Gives access to scene manager class used to load scenes

public class GameManager : MonoBehaviour
{
    public Ball ball { get; private set; }
    public Paddle paddle { get; private set; }
    public Bricks[] bricks { get; private set; }

    public int level = 1;
    public int score = 0;
    public int lives = 3;


    private void Awake()                        //Built-in Unity function, called automatically when script is initialized
    { 
        DontDestroyOnLoad(gameObject);     //Built-In Unity function which takes game object parameter (this is applied to the GameManager object). This is to keep game manager to persist and not be destroyed
        SceneManager.sceneLoaded += OnLevelLoaded;
    }

    private void Start()
    {
        NewGame();
    }

    private void NewGame() 
    {
        score = 0;
        lives = 3;

        LoadLevel(1);
    }

    private void LoadLevel(int level)
    {
        this.level = level;

        SceneManager.LoadScene("Level" + level);
    }

    private void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        ball = FindObjectOfType<Ball>();
        paddle = FindObjectOfType<Paddle>();
        bricks = FindObjectsOfType<Bricks>();
    }

    private void ResetLevel()
    {
        ball.ResetBall();
        paddle.ResetPaddle();
    }

    private void GameOver()
    {
        //SceneManager.LoadScene("GameOver");       //This is option for game over screen...
        NewGame();
    }

    public void Miss()
    {
       lives--;

       if (lives > 0) {
            ResetLevel();
       } else {
            GameOver();
       }
    }

    public void Hit(Bricks bricks)
    {
        this.score += bricks.points;
        if (Cleared()) {
            LoadLevel(level + 1);
        }
    }

    private bool Cleared()
    {
        for (int i = 0; i < bricks.Length; i++)
        {
            if (bricks[i].gameObject.activeInHierarchy && !bricks[i].unbreakable) {
                return false;
            }
        }
        return true;
    }

}
