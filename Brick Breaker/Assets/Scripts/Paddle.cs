using UnityEngine;

public class Paddle : MonoBehaviour
{
    public new Rigidbody2D rigidbody { get; private set; } //reference to this component to allow movement. Private set
    public Vector2 direction { get; private set;}  //checks and stores for input direction
    public float speed = 500f;
    public float maxBounceAngle = 75f; //this is used for OnCollision Method
    private Animator animator;
    private bool facingRight = true;

    private void Awake()
    {
        this.rigidbody = GetComponent<Rigidbody2D>();  //assigns rigid body component
    }

    public void Start()
    {
        ResetPaddle();
        animator = gameObject.GetComponent<Animator>();
    }

    public void ResetPaddle()
    {
        this.transform.position = new Vector2(0f, this.transform.position.y);       //Don't want to zero Vector2 because will put paddle in middle of screen. Must set new Vector2 to only center horizontally
        this.rigidbody.velocity = Vector2.zero;
    }

    private void Update() //Unity provided function that checks states at every frame. This is where to usually put movement controls, etc.
    {

        animator.SetBool("IsMoving", direction == Vector2.left || direction == Vector2.right);

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {               //if a-key or left arrow key, move left
            this.direction = Vector2.left;
        } else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {       //if d-key or right arrow key, move right
            this.direction = Vector2.right;
        } else {
            this.direction = Vector2.zero;
        }

        if(direction == Vector2.right && !facingRight)
        {
            Flip();
        }
        else if(direction == Vector2.left && facingRight)
        {
            Flip();
        }
    }

    private void FixedUpdate() //called at a fixed interval rather than at frames like Update()
    {
            if (this.direction != Vector2.zero){
                this.rigidbody.AddForce(this.direction * this.speed);
            }
    }

    //This method adds some calculations to enhance the way ball reflects off paddle- more dynamic, more control
    //Spefically detects when item (ball) collides with paddle
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Ball ball = collision.gameObject.GetComponent<Ball>();
        if (ball != null)                            //only if ball detected
        {
            Vector2 paddlePosition = this.transform.position;           //find position of paddle
            Vector2 contactPoint = collision.GetContact(0).point;   //find intial point of contact

            float offset = paddlePosition.x - contactPoint.x;       //calculates offset from center
            //otherCollider hold value of paddle length. Can be used to calculate -100% - 100% position with 0 at center
            float width = collision.otherCollider.bounds.size.x / 2;

            //Calculate angle and then rotation to redirect ball
            float currentAngle = Vector2.SignedAngle(Vector2.up, ball.rigidbody.velocity);
            float bounceAngle = (offset / width) * this.maxBounceAngle; //to keep from bouncing at 90 degrees
            float newAngle = Mathf.Clamp(currentAngle + bounceAngle, -this.maxBounceAngle, this.maxBounceAngle);    //Mathf.Clamp keeps the bounce angle within desired range

            Quaternion rotation = Quaternion.AngleAxis(newAngle, Vector3.forward); //WTH is quaternion???
            //sets rididbody to new behavior. Magnitude is velocity with only speed and not direction. 
            //new rotation (formed by angle) relative to y axis, scaled to existing speed of the ball (don't change speed)
            ball.rigidbody.velocity = rotation * Vector2.up * ball.rigidbody.velocity.magnitude;
        }
    }

    void Flip()
        {
            facingRight = !facingRight;
            Vector2 currentScale = transform.localScale;
            currentScale.x *= -1; //this flips the sprite
            transform.localScale = currentScale;
        }
    

}


/*
Notes:
Vector2 describes the x and y position of game object
Vector3 describes a 3-dimensional object's magnitude and direction info for all 3 dimensions
    In game development, Vector3 mostly used to find position of object or distance between objects
Quaternion: a complex number of the form w + xi + yj + zk, 
    where w, x, y, z are real numbers and i, j, k are imaginary units 
    that satisfy certain conditions.
Mathf is a collection of Unity math functions https://docs.unity3d.com/ScriptReference/Mathf.html#:~:text=Description,collection%20of%20common%20math%20functions.
Mathf.Clamp clamps the given value betwen the given min float and max flat values. Return the value if within the min max range
*/
